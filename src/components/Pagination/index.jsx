import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Pagination from '@material-ui/lab/Pagination';
import { useDispatch, useSelector } from 'react-redux';
import { changePaginationPage } from '../../redux/actions/loadingData';

const useStyles = makeStyles((theme) => ({
  root: {
    '& > * + *': {
      marginTop: theme.spacing(2),
    },
  },
}));
const limit = 20;
const PaginationRounded = () => {
  const { count, page } = useSelector((state) => state.dataRequest);
  const totalPaga = Math.ceil(count / limit);
  const dispatch = useDispatch();
  const classes = useStyles();
  const handleChange = (event, value) => {
    dispatch(changePaginationPage(value));
  };
  return (
    <div className={classes.root}>
      <Pagination count={totalPaga} page={page} onChange={handleChange} />
    </div>
  );
};

export default PaginationRounded;
