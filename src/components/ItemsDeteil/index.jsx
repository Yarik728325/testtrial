import React from 'react';
import { Card } from 'antd';

const { Meta } = Card;
const ItemsDeteils = ({ text, desc }) => {
  return (
    <>
      <Card
        hoverable
        style={{ width: 240 }}
        cover={
          <img
            alt="example"
            src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png"
          />
        }
      >
        <Meta title={text} description={desc} />
      </Card>
    </>
  );
};
export default ItemsDeteils;
