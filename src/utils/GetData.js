export async function tryLogin({ url, data, check }) {
  let response = await fetch(url, {
    method: 'POST',
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
  });
  if (check) {
    response = await response.json();
  }

  return response;
}

export async function getData({ url, token }) {
  let response = await fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Authorization: `Bearer  ${token}`,
    },
  });
  response = await response.json();
  return response;
}
