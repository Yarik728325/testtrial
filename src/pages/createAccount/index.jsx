import React from 'react';
import './style.scss';
import { Form, Input, Button, Checkbox } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { loginUserCreate } from '../../redux/actions/create';

const CreateAccount = () => {
  const { loading, isBusy } = useSelector((state) => state.createUser);

  const dispatch = useDispatch();
  const onFinish = (values) => {
    console.log(values);
    dispatch(loginUserCreate(values));
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };
  if (loading) {
    return <h1>Loading....</h1>;
  }
  let checker = '';
  if (isBusy && isBusy !== null) {
    checker = 'We already have such login';
  }
  return (
    <div className="wrapper">
      <Form
        name="basic"
        labelCol={{
          span: 8,
        }}
        wrapperCol={{
          span: 16,
        }}
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item
          label="Username"
          name="username"
          rules={[
            {
              required: true,
              message: 'Please input your username!',
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Password"
          name="password"
          rules={[
            {
              required: true,
              message: 'Please input your password!',
            },
          ]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item
          name="remember"
          valuePropName="checked"
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <Checkbox>Remember me</Checkbox>
        </Form.Item>

        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
          className="OwnStyle"
        >
          <Button type="primary" htmlType="submit" className="ownButton">
            createAccount
          </Button>
        </Form.Item>
        <h3>{checker}</h3>
      </Form>
    </div>
  );
};

export default CreateAccount;
