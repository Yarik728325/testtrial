import React from 'react';
import './style.scss';
import history from '../../utils/routing';

const SuccessCreate = () => {
  return (
    <div className="card">
      <div className="wrapped">
        <i className="checkmark">✓</i>
      </div>
      <h1>Success</h1>
      <p>You can login with your data</p>
      <button
        type="button"
        onClick={() => {
          history.push('/login');
        }}
      >
        LogIn
      </button>
    </div>
  );
};

export default SuccessCreate;
