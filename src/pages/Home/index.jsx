import React from 'react';
import { useSelector } from 'react-redux';
import './style.scss';
import AuthUser from '../AuthUser';
import Login from '../Login';

const Home = () => {
  const { isAuth } = useSelector((state) => state.login);
  if (!isAuth) {
    return <Login />;
  }
  return (
    <>
      <AuthUser />
    </>
  );
};

export default Home;
