import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
// eslint-disable-next-line import/no-unresolved
import PaginationRounded from '../../components/Pagination';
import Header from '../../components/Header';
import { loadingData } from '../../redux/actions/loadingData';
import ItemsDeteils from '../../components/ItemsDeteil';
import './style.scss';
import history from '../../utils/routing';

const AuthUser = () => {
  const dispatch = useDispatch();
  const { token } = useSelector((state) => state.login);
  const { page } = useSelector((state) => state.dataRequest);
  useEffect(() => {
    dispatch(loadingData({ token, page }));
  }, [dispatch, token, page]);
  let { data } = useSelector((state) => state.dataRequest);
  if (data !== null) {
    data = data.map((e) => {
      return (
        <ItemsDeteils
          text={e.text}
          desc={e.desc}
          key={Math.random().toString(36).substring(7)}
        />
      );
    });
  }
  if (data == null) {
    return <h1>Loading</h1>;
  }
  return (
    <>
      <div className="AuthUser">
        <Header />
        <button onClick={()=>{history.push("/check")}}>Checker</button>
        <div className="wrapper_auth">{data}</div>
        <PaginationRounded />
      </div>
    </>
  );
};

export default AuthUser;