import React from 'react';
import { Route, Router } from 'react-router';
import history from '../utils/routing';
import Login from '../pages/Login';
import CreateAccount from '../pages/createAccount';
import SuccessCreate from '../pages/SuccessCreate';
import Home from '../pages/Home';

const routes = [
  {
    id: 'main',
    path: '/',
    exact: true,
    component: Home,
  },
  {
    id: 'login',
    path: '/login',
    exact: true,
    component: Login,
  },
  {
    id: 'createAccout',
    path: '/createAccount',
    exact: true,
    component: CreateAccount,
  },
  {
    id: 'SuccessCreate',
    path: '/created',
    exact: true,
    component: SuccessCreate,
  },
];

const RouterSwitch = () => {
  return (
    <Router history={history}>
      {routes.map((e) => {
        const { id, ...props } = e;
        return <Route key={id} {...props} />;
      })}
    </Router>
  );
};

export default RouterSwitch;
