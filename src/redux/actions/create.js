import { createAction } from '../../utils/createAction';

export const loginUserCreate = createAction('LOGIN/CREATE');
export const loginUserSuccessCreate = createAction('LOGIN/SUCCESS/CREATE');
export const loginUserFailureCreate = createAction('LOGIN/FAILURE/CREATE');
