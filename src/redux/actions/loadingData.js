import { createAction } from '../../utils/createAction';

export const loadingData = createAction('DATA/LOADING');
export const successData = createAction('DATA/SUCCESS');
export const faulireData = createAction('DATA/FAILURE');
export const changePaginationPage = createAction('DATA/PAGINATION');
