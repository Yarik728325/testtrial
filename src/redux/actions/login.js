// eslint-disable-next-line import/no-unresolved
import { createAction } from '../../utils/createAction';

export const loginUser = createAction('LOGIN/BEGIN');
export const loginUserSuccess = createAction('LOGIN/SUCCESS');
export const loginUserFailure = createAction('LOGIN/FAILURE');
