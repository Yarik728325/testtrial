// eslint-disable-next-line import/no-unresolved
import {
  loginUserCreate,
  loginUserFailureCreate,
  loginUserSuccessCreate,
} from '../actions/create';

const initinalState = {
  isBusy: null,
  loading: false,
  error: false,
};

export default (state = initinalState, { type }) => {
  switch (type) {
    case loginUserCreate.type:
      return {
        ...state,
        loading: true,
      };
    case loginUserSuccessCreate.type:
      return {
        ...state,
        isBusy: false,
        loading: false,
      };
    case loginUserFailureCreate.type:
      return {
        ...state,
        loading: false,
        isBusy: true,
        error: true,
      };
    default:
      return state;
  }
};
