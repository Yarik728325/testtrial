// eslint-disable-next-line import/no-unresolved
import {
  loadingData,
  successData,
  faulireData,
  changePaginationPage,
} from '../actions/loadingData';

const initinalState = {
  data: null,
  count: null,
  page: 1,
  loading: false,
};

export default (state = initinalState, { type, payload }) => {
  switch (type) {
    case loadingData.type:
      return {
        ...state,
        loading: true,
      };
    case successData.type:
      return {
        ...state,
        data: payload.data,
        count: payload.count,
        loading: false,
      };
    case faulireData.type:
      return {
        ...state,
        loading: false,
        error: true,
      };
    case changePaginationPage.type:
      return {
        ...state,
        page: payload,
      };
    default:
      return state;
  }
};
