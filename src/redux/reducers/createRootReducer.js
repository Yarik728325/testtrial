import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

import login from './login';
import createUser from './createUser';
import dataRequest from './dataRequest';

export default (history) => combineReducers({
  login,
  createUser,
  dataRequest,
  router: connectRouter(history),
});
