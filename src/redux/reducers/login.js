// eslint-disable-next-line import/no-unresolved
import {
  loginUser,
  loginUserFailure,
  loginUserSuccess,
} from '../actions/login';

const initinalState = {
  isAuth: null,
  loading: false,
  error: false,
  token: null,
};

export default (state = initinalState, { type, payload }) => {
  switch (type) {
    case loginUser.type:
      return {
        ...state,
        loading: true,
      };
    case loginUserSuccess.type:
      return {
        ...state,
        isAuth: true,
        token: payload,
        loading: false,
      };
    case loginUserFailure.type:
      return {
        ...state,
        loading: false,
        isAuth: false,
        error: true,
      };
    default:
      return state;
  }
};
