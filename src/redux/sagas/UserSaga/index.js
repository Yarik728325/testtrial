import { takeLatest,takeEvery, put, call } from 'redux-saga/effects';
import { tryLogin } from '../../../utils/GetData';
import { loadingData, successData } from '../../actions/loadingData';
import { getData } from '../../../utils/GetData';
import {
  loginUser,
  loginUserSuccess,
  loginUserFailure,
} from '../../actions/login';
import {
  loginUserCreate,
  loginUserSuccessCreate,
  loginUserFailureCreate,
} from '../../actions/create';

import history from '../../../utils/routing';

export function* connectUser({ payload }) {
  const check = yield call(tryLogin, {
    url: 'http://141.95.55.203:8080/api/auth/login/',
    data: payload,
    check: true,
  });
  if (check.auth_token) {
    yield put(loginUserSuccess(check.auth_token));
    history.push('/');
  } else {
    yield put(loginUserFailure());
  }
}
export function* loadingTabsWorker({ payload }) {
  const { token, page } = payload;
  const request = yield call(getData, {
    url: `http://141.95.55.203:8080/api/models/?page=${page}`,
    token,
  });
  yield put(
    successData({
      data: [...request.results],
      count: request.count,
    }),
  );
}
export function* userCreate({ payload }) {
  const check = yield call(tryLogin, {
    url: 'http://141.95.55.203:8080/api/auth/users/',
    data: payload,
    check: false,
  });
  if (check.status === 500) {
    yield put(loginUserFailureCreate());
  } else {
    yield put(loginUserSuccessCreate());
    history.push('/created');
  }
}

export default function* watcherUser() {
  yield takeLatest(loginUser.type, connectUser);
  yield takeEvery(loadingData.type, loadingTabsWorker);
  yield takeEvery(loginUserCreate.type, userCreate);
}
